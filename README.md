# Gitlab CI Template

Jobs template pour les projets liés à Eiffage

## Jobs

### Commun

* `stages.gitlab-ci.yml`

|Nom|Description|
|--|--|
|`📡 install`|Phase d'installation|
|`🤞 test`|Phase de test|
|`📦 build`|Phase de build|
|`🫙 package`|Phase de packaging|
|`🚀 deploy`|Phase de déploiement|


* `variables.gitlab-ci.yml`

|Nom|Description|Valeur|
|--|--|--|
|`GIT_DEPTH`|Pour optimiser la CI|`1`|
|`FF_USE_FASTZIP`|Pour optimiser le cache|`true`|
|`ARTIFACT_COMPRESSION_LEVEL`|Pour optimiser le cache|`fast`|
|`CACHE_COMPRESSION_LEVEL`|Pour optimiser le cache|`fast`|

### Docker

* `node.gitlab-ci.yml`

  * Variables

  |Nom|Description|Valeur|Job|
  |--|--|--|--|
  |`NPM_GLOBAL_CACHE`|Répertoire pour le cache global pour npm|`/cache/.npm_global_cache`|--|
  |`NODE_IMAGE`|Définit l'image pour les jobs|`node:lts-alpine`|--|
  |`DOCKER_IMAGE`|Définit l'image pour les jobs|`docker:stable`|--|
  |`POSTFIX_CACHE`|Définit le postfix du nom du cache lié au build|`common`|`.node-build` `.node-docker`|
  |`TAG_NAME`|Définit un tag 'name' sur image docker|`latest`|` .node-docker`|
  |`DOCKERFILE_PATH`|Définit le path du Dockerfile|`.docker/Dockerfile`|`.node-docker`|
  |`DOCKERFILE_CONTEXT`|Définit le path du contexte d'execute du build Docker|`.`|`.node-docker`|
  |`CI_REGISTRY_USER`|Identifiant login pour se connecter au dépôt docker||
  |`CI_REGISTRY_PASSWORD`|Identifiant mot de passe pour se connecter au dépôt docker||
  |`CI_REGISTRY`|Dépôt distant docker||

  * Jobs

  |Nom|Stage|Image|Description|Commande|Obligation de réussir|Cache|
  |--|--|--|--|--|--|--|
  |`.node-install-deps`|`📡 install`|`$NODE_IMAGE`|Job pour installer les dépendances Node.js|`npm ci --cache $NPM_GLOBAL_CACHE --prefer-offline`|`oui`|`node_modules_cache`|
  |`.node-eslint`|`🤞 test`|`$NODE_IMAGE`|Job pour exécuter le linter|`npm run lint`|`oui`|`node_modules_cache`|
  |`.node-style-lint`|`🤞 test`|`$NODE_IMAGE`|Job pour exécuter le linter sur le style|`npm run lint:css`|`oui`|`node_modules_cache`|
  |`.node-prettier`|`🤞 test`|`$NODE_IMAGE`|Job pour exécuter le formateur prettier|`npm run prettier`|`oui`|`node_modules_cache`|
  |`.node-test`|`🤞 test`|`$NODE_IMAGE`|Job pour exécuter les tests unitaires|`npm run test`|`oui`|`node_modules_cache`|
  |`.node-test-coverage`|`🤞 test`|`$NODE_IMAGE`|Job pour exécuter les tests unitaires avec coverage|`npm run test:coverage`|`oui`|`node_modules_cache` `test_coverage_cache`|
  |`.node-audit-dependency`|`🤞 test`|`$NODE_IMAGE`|Job pour auditer les dépendances du projet|`npm audit`|`non`|`node_modules_cache`|
  |`.node-sonarqube-check`|`🦄 quality`|`sonarsource/sonar-scanner-cli:4.7`|Job pour exécuter l'analyse sonarqube|`sonar-scanner`|`non`|`node_modules_cache` `test_coverage_cache` `sonar_cache`|
  |`.node-build-test`|`📦 build`|`$NODE_IMAGE`|Job pour tester le build du projet|`npm run build`|`oui`|`node_modules_cache`|
  |`.node-build`|`📦 build`|`$NODE_IMAGE`|Job pour builder le projet|`npm run build`|`oui`|`node_modules_cache` `build`|
  |`.node-docker`|`🫙 package`|`$DOCKER_IMAGE`|Job pour packager le projet sous docker||`oui`|`node_modules_cache` `build`|
  |`.node-to-override`||`$DOCKER_IMAGE`|Job à écraser pour une tâche spécifique pour pouvoir utiliser le cache||`oui`|`node_modules_cache`|

  * Caches

  |Nom|Clé|Fichiers ou dossiers|Politique de cache|
  |--|--|--|--|
  |`node_modules_cache`|`fea`|`node_modules`|`pull`|
  |`build`|`build-$CI_PROJECT_ID-$CI_PIPELINE_ID-$POSTFIX_CACHE`|||

* `k8s.gitlab-ci.yml`

  * Variables

  |Nom|Description|Valeur|Job|
  |--|--|--|--|
  |`DEPLOY_IMAGE`|Définit l'image pour les jobs|`ronronan/kubectl-helm-client:latest`|--|
  |`HELM_CHART_REPO_URL`|Définit l'url du dépôt chart helm|`https://gitlab.com/api/v4/projects/41979763/packages/helm/stable`|--|
  |`HELM_CHART_REPO_NAME`|Définit le nom associé au dépôt chart helm|`helm-smart-platform`|--|
  |`HELM_CHART_NAME`|Définit le chart helm à déployer||--|
  |`HELM_NAMESPACE`|Définit le namespace Kubernetes où déployer le chart helm|`default`|--|
  |`IMAGE_VERSION`|Définit la version de l'image à déployer|`0.0.1`|`.k8s-helm-install`|
  |`KUBECONFIG`|Définit la configuration kubernetes à associé au job|`$KUBECONFIG`|`.k8s-helm-install`|
  |`HELM_REPO_LOGIN`|Identifiant login pour se connecter au dépôt chart helm|||
  |`HELM_REPO_PASSWORD`|Identifiant mot de passe pour se connecter au dépôt chart helm|||

  * Jobs

  |Nom|Stage|Image|Description|Commande|Obligation de réussir|Cache|
  |--|--|--|--|--|--|--|
  |`.k8s-helm-install`|`🚀 deploy`|`$DEPLOY_IMAGE`|||`oui`||

* `gitlab.gitlab-ci.yml`

  * Variables

  |Nom|Description|Valeur|Job|
  |--|--|--|--|
  |`TAG_NAME`|La valeur de la release généré dans Gitlab|`$CI_COMMIT_TAG`|`.gitlab-release`|
  * Jobs

  |Nom|Stage|Image|Description|Commande|Obligation de réussir|Cache|
  |--|--|--|--|--|--|--|
  |`gitlab-release`|`🔖 release`|`$DEPLOY_IMAGE`|Job pour générer une release sur le projet Gitlab||`oui`||
